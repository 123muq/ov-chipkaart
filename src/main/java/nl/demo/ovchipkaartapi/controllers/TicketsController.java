package nl.demo.ovchipkaartapi.controllers;

import nl.demo.ovchipkaartapi.core.Ticket;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class TicketsController {

    @GetMapping("/tickets/validate")
    public boolean validateTicket(String token) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @PostMapping("/tickets")
    @ResponseBody
    public Ticket issueNewTicket(@RequestParam String accountNumber, @RequestParam String from, @RequestParam String to) {
        return new Ticket(false, "", accountNumber, from, to);
    }

}
