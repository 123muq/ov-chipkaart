package nl.demo.ovchipkaartapi.services;

import nl.demo.ovchipkaartapi.core.Ticket;
import org.springframework.stereotype.Service;

@Service
public class TicketServiceImp implements TicketService {
    @Override
    public boolean validateTicket(String token) {
        return false;
    }

    @Override
    public Ticket issueNewTicket(String accountNumber, String from, String to) {
        return null;
    }
}
