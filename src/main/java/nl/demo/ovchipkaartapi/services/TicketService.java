package nl.demo.ovchipkaartapi.services;

import nl.demo.ovchipkaartapi.core.Ticket;

public interface TicketService {

    boolean validateTicket(String token);

    Ticket issueNewTicket(String accountNumber, String from, String to);
}
