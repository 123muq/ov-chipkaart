package nl.demo.ovchipkaartapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OvchipkaartApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(OvchipkaartApiApplication.class, args);
	}

}
