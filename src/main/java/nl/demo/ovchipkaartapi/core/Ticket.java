package nl.demo.ovchipkaartapi.core;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Ticket {
    private boolean valid;
    private String token;
    private String accountNumber;
    private String from;
    private String to;
}
