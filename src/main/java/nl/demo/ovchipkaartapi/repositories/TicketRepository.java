package nl.demo.ovchipkaartapi.repositories;

import nl.demo.ovchipkaartapi.core.Ticket;

public interface TicketRepository {

    Ticket create();

    Ticket findById();
}
